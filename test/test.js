var assert = require('assert')
var VersionSorter = require('../')

describe('VersionSorter', function() {
  describe('#sort()', function() {
    it('should sort versions correctly', function() {
      var versions = ["1.0.9", "1.0.10", "2.0", "3.1.4.2", "1.0.9a"]
      var sorted_versions = ["1.0.9", "1.0.9a", "1.0.10", "2.0", "3.1.4.2"]
      assert.deepEqual(VersionSorter.sort(versions), sorted_versions)
    })
    it('should return same object', function() {
      var versions = ['2.0', '1.0', '0.5']
      var sorted = VersionSorter.sort(versions)
      assert.strictEqual(versions, sorted)
    })
  })
  describe('#rsort()', function() {
    it('should sort versions correctly', function() {
      var versions = ["1.0.9", "1.0.10", "2.0", "3.1.4.2", "1.0.9a"]
      var sorted_versions = ["3.1.4.2", "2.0", "1.0.10", "1.0.9a", "1.0.9"]
      assert.deepEqual(VersionSorter.rsort(versions), sorted_versions)
    })
    it('should return same object', function() {
      var versions = ['2.0', '1.0', '0.5']
      var rsorted = VersionSorter.rsort(versions)
      assert.strictEqual(versions, rsorted)
    })
  })
})
