var VersionSorter = require('./build/Release/VersionSorter');

VersionSorter.rsort = function(versions) {
  VersionSorter.sort(versions);
  versions.reverse();
  return versions;
}

module.exports = VersionSorter;
