VersionSorter
=============

This module is a NodeJS port of [Ruby version By pope](https://github.com/pope/version_sorter).

```
var VersionSorter = require('version-sorter')
var versions = ['1.0.9', '2.0', '1.0.10', '1.0.3']
VersionSorter.rsort(versions) // ["2.0", "1.0.10", "1.0.9", "1.0.3"]
VersionSorter.sort(versions)  // ["1.0.3", "1.0.9", "1.0.10", "2.0"]
```

Great thanks to [@pope](https://github.com/pope)

Install
-------

    $ npm install version-sorter

