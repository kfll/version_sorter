#include <stdlib.h>
#include <string.h>
#include <nan.h>
#include "version_sorter.h"

using namespace v8;

char *StringValuePtr(Handle<String> string) {
    String::Utf8Value str (string);
    char *tmp = (char *)malloc(str.length() + 1);
    strcpy(tmp, *str);
    return tmp;
}

void Sort(const Nan::FunctionCallbackInfo<v8::Value>& args) {
    Nan::HandleScope scope;

    if (args.Length() != 1) {
        Nan::ThrowError("1 argument is expected, but wrong number of arguments is given.");
        args.GetReturnValue().SetUndefined();
    }

    if (args[0]->IsArray() == false) {
        Nan::ThrowError("Array is expected.");
        args.GetReturnValue().SetUndefined();
    }

    Local<Array> orig = args[0].As<Array>();
    uint32_t len = orig->Length();
    uint32_t i;
    char **c_list = (char**)calloc(len, sizeof(char *));

    for (i = 0; i < len; i++) {
        c_list[i] = StringValuePtr(orig->Get(Nan::New<Uint32>(i))->ToString());
    }

    version_sorter_sort(c_list, len);
    for (i = 0; i < len; i++) {
        Nan::ForceSet(orig, Nan::New<Uint32>(i), Nan::New(c_list[i]).ToLocalChecked());
        free(c_list[i]);
    }
    free(c_list);

    args.GetReturnValue().Set(orig);
}

NAN_MODULE_INIT(Init) {
    Nan::Set(target, Nan::New("sort").ToLocalChecked(),
    Nan::GetFunction(Nan::New<FunctionTemplate>(Sort)).ToLocalChecked());
}

NODE_MODULE(VersionSorter, Init);
