// https://gist.github.com/defunkt/209422
var fs = require('fs');
var VersionSorter = require('../');
suite('VersionSorter', function() {
  set('iterations', 20)

  var versions = fs.readFileSync(require('path').resolve(__dirname, 'tags.txt'), 'utf-8').split('\n');

  var times = 100;

  bench('#sort()', function() {
    for (i=0;i<times;i++)
      VersionSorter.sort(versions.slice());
  })
  bench('#rsort()', function() {
    for (i=0;i<times;i++)
      VersionSorter.rsort(versions.slice());
  })
})
